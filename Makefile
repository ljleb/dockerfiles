CONFIG_PATH := config
IMAGES := $(notdir $(wildcard $(CONFIG_PATH)/*))
DOCKERFILES := $(wildcard $(CONFIG_PATH)/*/Dockerfile)
MAKE_IMAGE := make -f ../../make/image.mk

include make/common-functions.mk

.PHONY: install
install: $(IMAGES)

.PHONY: $(IMAGES)
$(IMAGES):
	@$(MAKE_IMAGE) -C $(CONFIG_PATH)/$@ install

include gen/make/dependencies.mk
gen/make/dependencies.mk: $(DOCKERFILES)
	@CONFIG_PATH=$(CONFIG_PATH) sh make/generate-dependencies.sh $@ $^

.PHONY: uninstall
uninstall: $(call uninstall,$(IMAGES))

.PHONY: $(call uninstall,$(IMAGES))
$(call uninstall,$(IMAGES)):
	@$(MAKE_IMAGE) -C $(CONFIG_PATH)/$(patsubst uninstall-%,%,$@) uninstall

.PHONY: new
new:
	@CONFIG_PATH=$(CONFIG_PATH) sh make/new-image.sh
