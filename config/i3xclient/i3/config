#!/bin/bash
# i3-config-wizard(1) = reset i3
# http://i3wm.org/docs/userguide.html = complete reference

# setup variables

    # feh
        set $BG_IMAGE_DURATION 5
        set $WIDTH $(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1)
        set $HEIGHT $(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2)

    # gaps
        set $OUTER_GAPS 0
        set $INNER_GAPS 0

    # volume
        set $VOLUME_SPEED 5%

    # brightness
        set $BRIGHTNESS_PATH /sys/class/backlight/intel_backlight/brightness
        set $BRIGHTNESS_MAXIMUM 937
        set $BRIGHTNESS_MINIMUM 1
        set $BRIGHTNESS_SPEED 936

    # control keys
        set $shift Shift
        set $mod Mod4
        set $alt Mod1
        set $alt2 Mod2
        set $ctrl Control

# setup background
    # enable network manager
        exec --no-startup-id nm-applet

    # enable low battery alert
        exec --no-statup-id i3-battery-popup -t 30s -L 27 -n -s ~/src/i3-battery-popup/i3-battery-popup.wav

    # fill Origin with its contents?
        for_window [instance="Origin.exe"] floating enable

# setup foreground
    # Font for window titles. Will also be used by the bar unless a different font is used in the bar {} block below.
        #font pango:menlo 8

    # start i3bar to display a workspace bar (plus the system information i3status finds out, if available)
        bar {
            status_command i3status
            tray_output primary
        }

    # i3 colors
        # class                 border  backgr. text    indicator child_border
        client.focused          #4c7899 #285577 #ffffff #2e9ef4   #285577
        client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
        client.unfocused        #333333 #222222 #888888 #292d2e   #222222
        client.urgent           #2f343a #900000 #ffffff #900000   #900000
        client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c
        client.background       #ffffff

    # i3-gaps border settings
        #for_window [class="^.*"] border pixel 1
        #gaps inner $INNER_GAPS
        #gaps outer $OUTER_GAPS

    # set background image
        #exec watch -n $BG_IMAGE_DURATION feh --randomize --bg-fill ~/images/backgrounds
        exec --no-startup-id feh --bg-fill $I3_CONFIG/share/backgrounds/default --geometry 1920x1080-1000-1000

    # enable transparency
        exec --no-startup-id compton --config /etc/home/compton/compton.conf

    # get rid of the mouse (move to lower right corner)
        #exec unclutter --grab

# setup shortcuts
        # take a screenshot
            bindsym Print exec --no-startup-id scrot /tmp/%s.png

        # start a terminal
            bindsym $mod+Return exec --no-startup-id i3-sensible-terminal

        # start web-browser
            bindsym $mod+w exec --no-startup-id firefox

        # start file-browser
            bindsym $mod+e exec --no-startup-id nautilus

        # start messenger
            bindsym $mod+m exec --no-startup-id ~/bin/messenger-linux-x64/messenger

        # kill a window
            bindsym $mod+BackSpace kill

        # lock i3
            bindsym $mod+$shift+l exec --no-startup-id ~/.config/i3lock/i3lock blur

        # suspend
            bindsym $mod+$ctrl+Delete exec systemctl suspend

        # log out
            bindsym $mod+$alt+Delete exec i3 exit

        # shutdown
            bindsym $mod+$shift+Delete exec poweroff

        # reload the configuration file
            bindsym $mod+$ctrl+r reload

        # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
            bindsym $mod+$shift+r restart

        # change master speaker volume
            bindsym $mod+KP_Add exec amixer sset Master $VOLUME_SPEED+
            bindsym $mod+KP_Subtract exec amixer sset Master $VOLUME_SPEED-

        # change brightness
            bindsym $mod+$alt+KP_Add exec --no-startup-id "echo $(($(cat $BRIGHTNESS_PATH) + $BRIGHTNESS_SPEED > $BRIGHTNESS_MAXIMUM ? $BRIGHTNESS_MAXIMUM : $(cat $BRIGHTNESS_PATH) + $BRIGHTNESS_SPEED)) > $BRIGHTNESS_PATH"
            bindsym $mod+$alt+KP_Subtract exec --no-startup-id "echo $(($(cat $BRIGHTNESS_PATH) - $BRIGHTNESS_SPEED < $BRIGHTNESS_MINIMUM ? $BRIGHTNESS_MINIMUM : $(cat $BRIGHTNESS_PATH) - $BRIGHTNESS_SPEED)) > $BRIGHTNESS_PATH"

        # change focus
            bindsym $mod+Left focus left
            bindsym $mod+Down focus down
            bindsym $mod+Up focus up
            bindsym $mod+Right focus right

        # move focused window
            bindsym $mod+$shift+Left move left
            bindsym $mod+$shift+Down move down
            bindsym $mod+$shift+Up move up
            bindsym $mod+$shift+Right move right

        # change workspace
            # as a linked list
                bindsym $mod+$alt+Right exec --no-startup-id "$I3_CONFIG/workspace/increment.sh"
                bindsym $mod+$alt+Left exec --no-startup-id "$I3_CONFIG/workspace/decrement.sh"

            # as a random access list
                bindsym $mod+grave exec --no-startup-id "$I3_CONFIG/workspace/set.sh 0"
                bindsym $mod+1 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 1"
                bindsym $mod+2 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 2"
                bindsym $mod+3 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 3"
                bindsym $mod+4 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 4"
                bindsym $mod+5 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 5"
                bindsym $mod+6 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 6"
                bindsym $mod+7 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 7"
                bindsym $mod+8 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 8"
                bindsym $mod+9 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 9"
                bindsym $mod+0 exec --no-startup-id "$I3_CONFIG/workspace/set.sh 10"

        # move focused window to workspace
            # as a linked list
                bindsym $mod+$shift+$alt+Right exec --no-startup-id $I3_CONFIG/container/increment.sh
                bindsym $mod+$shift+$alt+Left exec --no-startup-id $I3_CONFIG/container/decrement.sh

            # as a random access list
                bindsym $mod+$shift+grave move container to workspace 0
                bindsym $mod+$shift+1 move container to workspace 1
                bindsym $mod+$shift+2 move container to workspace 2
                bindsym $mod+$shift+3 move container to workspace 3
                bindsym $mod+$shift+4 move container to workspace 4
                bindsym $mod+$shift+5 move container to workspace 5
                bindsym $mod+$shift+6 move container to workspace 6
                bindsym $mod+$shift+7 move container to workspace 7
                bindsym $mod+$shift+8 move container to workspace 8
                bindsym $mod+$shift+9 move container to workspace 9
                bindsym $mod+$shift+0 move container to workspace 10

        # toggle split in horizontal or vertical orientation
            bindsym $mod+h split toggle

        # enter fullscreen mode for the focused container
            bindsym $mod+f fullscreen toggle

        # toggle tiling / floating
            bindsym $mod+$shift+space floating toggle

        # change focus between tiling / floating windows
            bindsym $mod+space focus mode_toggle

        # focus the parent container
            bindsym $mod+a focus parent

    # workspace setup
        exec --no-startup-id i3-msg 'workspace 0; exec i3-sensible-terminal'

    # start messenger
        exec --no-startup-id messenger
